const express = require('express');
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require('mongodb').ObjectId;

//pkt7
recordRoutes.route("/products/report").get(function (req, res) {
    let db_connect = dbo.getDb("food");
    db_connect.collection("products").aggregate([
        {
            $group: {
                _id: "$nazwa",
                totalQuantity: { $sum: "$ilość" }
            }
        },
        {
            $project: {
                _id: 0,
                nazwa: "$_id",
                ilość: "$totalQuantity"
            }
        }
    ]).toArray(function (err, result) {
        if (err) {
            throw err;
        } else {
            if (result.length === 0) {
                res.json({ message: "Brak danych w magazynie." });
            } else {
                let totalQuantity = result.reduce((acc, product) => acc + product.ilość, 0);
                res.json({ totalQuantity, products: result });
            }
        }
    });
});


//pkt6
recordRoutes.route("/products/:id").delete(function (req, res) {
    let db_connect = dbo.getDb("food");
    let myquery = { _id: ObjectId(req.params.id) };
    db_connect.collection("products").deleteOne(myquery, function (err, result) {
        if (err) {
            console.error("Błąd podczas usuwania produktu:", err);
        } else {
            if (result.deletedCount === 1) {
                res.json({ message: "Produkt został pomyślnie usunięty." });
            } else {
                res.json({ message: "Nie znaleziono produktu o podanym identyfikatorze." });
            }
        }
    });
});

//pkt5
recordRoutes.route("/products/:id").put(function(req, res) {
    let db_connect = dbo.getDb("food");
    let myquery = {_id: ObjectId(req.params.id)};
    let newValues = {
        $set: {
            nazwa: req.body.nazwa,
            cena: req.body.cena,
            opis: req.body.opis,
            ilość: req.body.ilość,
            jednostka_miary: req.body.jednostka_miary
        }
    };
    db_connect.collection("products").updateOne(myquery, newValues, function(err, result){
        if (err) {
            throw err;
        } else {
            if (result.modifiedCount === 1) {
                res.json({ message: "Produkt został zaktualizowany." });
            } else {
                res.json({ message: "Nie znaleziono produktu o podanym identyfikatorze." });
            }
        }
    });
});

//pkt4
recordRoutes.route("/products").post(function(req, res) {
    let db_connect = dbo.getDb("food");
    let myobj = {
        nazwa: req.body.nazwa,
        cena: req.body.cena,
        opis: req.body.opis,
        ilość: req.body.ilość,
        jednostka_miary: req.body.jednostka_miary
    };
    db_connect.collection("products").insertOne(myobj, function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

//pkt3
recordRoutes.route("/products").get(function(req, res) {
    let db_connect = dbo.getDb("food");
    let myQuery = {};
    let sortOptions = {};

    const filterParams = ['nazwa', 'cena', 'opis', 'ilość', 'jednostka_miary'];
    filterParams.forEach(param => {
        if (req.query[param]) {
            if (param === 'cena') {
                myQuery[param] = parseFloat(req.query[param]);
            } else if (param === 'ilość') {
                myQuery[param] = parseInt(req.query[param]);
            } else {
                myQuery[param] = req.query[param];
            }
        }
    });

    if (req.query.sortBy) {
        sortOptions[req.query.sortBy] = (req.query.sortOrder == -1) ? -1 : 1;
    }

    db_connect.collection("products").find(myQuery).sort(sortOptions).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});
module.exports = recordRoutes;